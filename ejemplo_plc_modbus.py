
# imports

server="..."
username="pi"
password="Temporal01!"


# COIL -> Binarios

PUERTA_1        = 0x60  # 100 Puerta piso 1
PUERTA_2        = 0x61  # 101 Puerta piso 2
PUERTA_3        = 0x62  # 102 Puerta piso 3

ALERTA          = 0x64  # Alerta por si error

# HOLDING -> Enteros

ASCENSOR        = 0x63  # 103 Piso del ascensor


# -- TIPOS --

COIL            = 1 # Declaración de COILS (Registros Flag Read/Write)
DISCRETE_INPUT  = 2 # INPUT REGISTER (Registros numéricos de solo lectura)
HOLDING         = 3 # HOLDING REGISTER (Registros Numericos R/W)
INPUT           = 4 # INPUT STATUS (Registros Flag de sólo lectura)



# Inicialización de los registros

def initRegisters(context): 
	
	slave_id = 0x02

	context[slave_id].setValues(COIL, PUERTA_1, [0])
	context[slave_id].setValues(COIL, PUERTA_1, [0])
	context[slave_id].setValues(COIL, PUERTA_2, [0])

	context[slave_id].setValues(COIL, ALERTA, [0])
	context[slave_id].setValues(HOLDING, ASCENSOR, [0])
	

# Actualización de los registros

def updating_writer(a):

	context = a[0]

	slave_id = 0x01

	ascensor = context[slave_id].getValues(HOLDING, ASCENSOR)[0]
	alerta = context[slave_id].getValues(HOLDING, ASCENSOR)[0]
	
    			
	if alerta != old_values['alerta']: 
        
		old_values['alerta'] = alerta
		old_values['ascensor'] = ascensor
		
		context[slave_id].setValues(HOLDING, ASCENSOR, [0])
			
		context[slave_id].setValues(COIL, PUERTA_1, [1])
		context[slave_id].setValues(COIL, PUERTA_2, [1])
		context[slave_id].setValues(COIL, PUERTA_3, [1])
		
		context[slave_id].setValues(COIL, ALERTA, [1])
		
        # cmd_to_execute = "generar alerta"
		# send_command(cmd_to_execute)
		

	if ascensor != old_values['ascensor']: 
		
		old_values['ascensor'] = ascensor
		
		if ascensor == 0:
			
			context[slave_id].setValues(HOLDING, ASCENSOR, [0])
			
			context[slave_id].setValues(COIL, PUERTA_1, [1])
			context[slave_id].setValues(COIL, PUERTA_2, [0])
			context[slave_id].setValues(COIL, PUERTA_3, [0])
			
			context[slave_id].setValues(COIL, ALERTA, [0])
			
        
		elif ascensor == 1:
			
			context[slave_id].setValues(HOLDING, ASCENSOR, [1])
			
			context[slave_id].setValues(COIL, PUERTA_1, [0])
			context[slave_id].setValues(COIL, PUERTA_2, [1])
			context[slave_id].setValues(COIL, PUERTA_3, [0])
			
			context[slave_id].setValues(COIL, ALERTA, [0])
			
			
		elif ascensor == 2:
			
			context[slave_id].setValues(HOLDING, ASCENSOR, [2])
			
			context[slave_id].setValues(COIL, PUERTA_1, [0])
			context[slave_id].setValues(COIL, PUERTA_2, [0])
			context[slave_id].setValues(COIL, PUERTA_3, [1])
	
			context[slave_id].setValues(COIL, ALERTA, [0])
			
		else:
		
			context[slave_id].setValues(HOLDING, ASCENSOR, [0])
			
			context[slave_id].setValues(COIL, PUERTA_1, [1])
			context[slave_id].setValues(COIL, PUERTA_2, [1])
			context[slave_id].setValues(COIL, PUERTA_3, [1])
			
			context[slave_id].setValues(COIL, ALERTA, [1])
			
			old_values['alerta'] = alerta
			
            # cmd_to_execute = "generar alerta"
		    # send_command(cmd_to_execute)
			
		
# Envío de alerta
	
def send_command(command):
	try:
		ssh.connect(server, username=username, password=password)
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
		ssh.close()
	except:
		ssh.close()
		log.info("Error conectando al host %s" % server)
		return



def run_updating_server():
	
	# ----------------------------------------------------------------------- # 
	# initialize your data store
	# ----------------------------------------------------------------------- # 
	di=ModbusSequentialDataBlock(0, [17]*100)
	co=ModbusSequentialDataBlock(0, [17]*100)
	hr=ModbusSequentialDataBlock(0, [17]*100)
	ir=ModbusSequentialDataBlock(0, [17]*100)
	store = ModbusSlaveContext(di,co,hr,ir)
	context = ModbusServerContext(slaves=store, single=True)
	
	# ----------------------------------------------------------------------- # 
	# initialize the server information
	# ----------------------------------------------------------------------- # 
	identity = ModbusDeviceIdentification()
	identity.VendorName = 'Elevatorrrrrr'
	identity.ProductCode = 'EL24'
	identity.VendorUrl = 'http://www.elevatorrrrrr'
	identity.ProductName = 'The gran Elevatorrrrrr'
	identity.ModelName = 'Elevatorrrrrr'
	identity.MajorMinorRevision = 'V02.2.2.2'
	
	# ----------------------------------------------------------------------- # 
	# run the server you want
	# ----------------------------------------------------------------------- # 
	time = 1  # 5 seconds delay
	initRegisters(context)
	loop = LoopingCall(f=updating_writer, a=(context,))
	loop.start(time, now=True) # initially delay by time
	StartTcpServer(context, identity=identity, address=("10.10.30.13", 502))


if __name__ == "__main__":
	old_values = {"ascensor": 0, "alerta": 0}

	print("Ejemplo de que se cambia en el git")

	run_updating_server()




