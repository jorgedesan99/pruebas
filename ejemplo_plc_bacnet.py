
# imports

server = "..."
username = "pi"
password = "Temporal01!"

# Iniciar Servicio

def start_device(ip, deviceId):
	try:
		new_device = BAC0.lite(ip=ip, deviceId=deviceId, localObjName="Elevator Controller", description="https://www.intesis.com/products/ac-interfaces/bacnet-gateways/daikin-bacnet-ac-dk-ac-bac-1?ordercode=INBACDAI001I000", modelName="Daikin AC Domestic Units to BACnetIP/MSTP") #El nombre es personalizable
	except Exception:
		new_device = BAC0.lite(deviceId=deviceId, localObjName="Elevator Controller", description="https://www.intesis.com/products/ac-interfaces/bacnet-gateways/daikin-bacnet-ac-dk-ac-bac-1?ordercode=INBACDAI001I000", modelName="Daikin AC Domestic Units to BACnetIP/MSTP") #El nombre es personalizable
	time.sleep(1)

	# Registramos los tipos de objeto con los que vamos a trabajar.
	register_object_type(AnalogOutputCmdObject, vendor_id=842) # Solo lectura
	register_object_type(AnalogValueCmdObject, vendor_id=842)
	register_object_type(BinaryOutputCmdObject, vendor_id=842)
	register_object_type(BinaryValueCmdObject, vendor_id=842) # Solo lectura


    # Ascensor

	ascensor = AnalogValueCmdObject(
		objectIdentifier=("analogValue", 1),
		objectName="Elevator",
		presentValue=0,
		description=CharacterString("Elevator floor")
	)
	
    # Alerta

	alerta = BinaryValueCmdObject(
		objectIdentifier=("binaryValue", 1),
		objectName="Alert",
		presentValue='inactive',
		description=CharacterString("Alert")
	)
	
	# Puertas pisos
 
	puerta_1 = BinaryValueCmdObject(
		objectIdentifier=("binaryValue", 2),
		objectName="Floor_1",
		presentValue='inactive',
		description=CharacterString("Floor 1 door")
	)

	puerta_2 = BinaryValueCmdObject(
		objectIdentifier=("binaryValue", 3),
		objectName="Floor_2",
		presentValue='inactive',
		description=CharacterString("Floor 2 door")
	)
	
	puerta_3 = BinaryValueCmdObject(
		objectIdentifier=("binaryValue", 4),
		objectName="Floor_3",
		presentValue='inactive',
		description=CharacterString("Floor 3 door")
	)



	# Añadimos los objetos a nuestro PLC
	new_device.this_application.add_object(ascensor)
	new_device.this_application.add_object(alerta)
	new_device.this_application.add_object(puerta_1)
	new_device.this_application.add_object(puerta_2)
	new_device.this_application.add_object(puerta_3)
	
	elements = ['Elevator', 'Alert', 'Floor_1', 'Floor_2', 'Floor_3']

	return new_device, elements




def update(args):
	#Update the outside temp
	try:
		device = args['device']
		elements = args['elements']
		old_values = args['old_values']
		
		ascensor = device.this_application.get_object_name('Elevator').presentValue
		alerta = device.this_application.get_object_name('Alert').presentValue
		
		if alerta != old_values['alert']:
			
			device.this_application.get_object_name("Elevator").presentValue = 0
                
			device.this_application.get_object_name("Floor_1").presentValue = 'active'
			device.this_application.get_object_name("Floor_2").presentValue = 'active'
			device.this_application.get_object_name("Floor_3").presentValue = 'active'
			device.this_application.get_object_name("Alert").presentValue = 'active'
				
			old_values['alert'] = alerta
			old_values['ascensor'] = ascensor
			

		if ascensor != old_values['ascensor']:
			
			old_values['ascensor'] = ascensor
			
			if ascensor == 0:	
				device.this_application.get_object_name("Elevator").presentValue = 0
                
				device.this_application.get_object_name("Floor_1").presentValue = 'active'
				device.this_application.get_object_name("Floor_2").presentValue = 'inactive'
				device.this_application.get_object_name("Floor_3").presentValue = 'inactive'
				device.this_application.get_object_name("Alert").presentValue = 'inactive'
				
			elif ascensor == 1:	
				device.this_application.get_object_name("Elevator").presentValue = 1
                
				device.this_application.get_object_name("Floor_1").presentValue = 'inactive'
				device.this_application.get_object_name("Floor_2").presentValue = 'active'
				device.this_application.get_object_name("Floor_3").presentValue = 'inactive'
				device.this_application.get_object_name("Alert").presentValue = 'inactive'
				
			elif ascensor == 2:	
				device.this_application.get_object_name("Elevator").presentValue = 2
                
				device.this_application.get_object_name("Floor_1").presentValue = 'inactive'
				device.this_application.get_object_name("Floor_2").presentValue = 'inactive'
				device.this_application.get_object_name("Floor_3").presentValue = 'active'
				device.this_application.get_object_name("Alert").presentValue = 'inactive'
				
			else:
				device.this_application.get_object_name("Elevator").presentValue = 0
                
				device.this_application.get_object_name("Floor_1").presentValue = 'active'
				device.this_application.get_object_name("Floor_2").presentValue = 'active'
				device.this_application.get_object_name("Floor_3").presentValue = 'active'
				device.this_application.get_object_name("Alert").presentValue = 'active'
				
				old_values['alert'] = alerta
				
			set_oldValues(old_values, device)

		
		#send_command()
		print("Valores Actuales:\n")
		for e in elements:
			currentValue = device.this_application.get_object_name(e).presentValue
			print("%s = %s\n" %(e, currentValue))
	except Exception as e:
		print("ERROR: %s" % e)

def set_oldValues(old_values, device):
	old_values['ascensor'] = device.this_application.get_object_name("Elevator").presentValue
	old_values['alerta'] = device.this_application.get_object_name("Alert").presentValue
	

def send_command(color, brightness):
	cmd_to_execute = "Comando"
	print(cmd_to_execute)
	ssh.connect(server, username=username, password=password)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd_to_execute)
	ssh.close()


if __name__ == '__main__':
	#Inicializamos el dispositivo con la IP y el ID numérico para el PLC
	device, elements = start_device("10.10.30.10", "100")
	#Definimos la funcion recurrente a la que vamos a llamar
	old_values = {'ascensor':0, 'alerta':'inactive'}

	while(True):
		update({"device":device, "elements": elements, "old_values":old_values})
		time.sleep(0.5)
				
